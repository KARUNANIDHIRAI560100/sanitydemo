export default {
    name: 'userTitle',
    title: 'User Registration',
    type: 'document',
    fields: [
      {
        name: 'userFullName',
        title: 'Full Name',
        type: 'string',
        validation: Rule => [
            Rule.required().min(5).error('User Name of min. 10 characters is required'),
            Rule.max(20).warning('Shorter Name are usually better')
          ]
      },
      {
        name: 'UserEmail',
        title: 'Email',
        type: 'string',
        initialValue: 'xxxxxxx@xxxxx.com',
      },
      {
        title: 'Mobile',
        name: 'mobileArray',
        type: 'array',
        of: [
            {type: 'string'},
        ],
      },
      {
        name: 'Address',
        title: 'Address',
        type: 'string',
      },
      {
        name: 'City',
        title: 'City',
        type: 'string',
      },
      {
        name: 'Country',
        title: 'Country',
        type: 'string',
      },
      {
        name: 'PinCode',
        title: 'Pin Code',
        type: 'string',
      },

    ],
  }
  